/*
Felix Quintana
Prof. Chen
Hw03 2/10/18
TThe objective  of this code is to figure out how much volume in cubic miles
givien the rainfall in inches and acres.
*/
import java.util.Scanner;
public class Convert{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);  // defining my scanner and then take inputs of acres and rain seen 
    System.out.print("Enter the affected area in acres: ");
    double acres = input.nextDouble();
    System.out.print("Enter the amount of rain in inches in the form xx.xx ");
    double inchesrain = input.nextDouble();
    double gallons = inchesrain*acres*27154.285990761;//acres inches to gallons converstion
    double cubicmiles = gallons/1.1011e+12;// straight up converstion from gallons to cubicmiles
    System.out.println("rain in cubic meters: "+cubicmiles);   
  }//end of main
}//end of class