/*
Felix Quintana
Prof Chen
Pyramid hw03
Objective is to calculate the volume of a pyramid given the height and the with and length
*/
import java.util.Scanner;
public class Pyramid{
  public static void main(String[] args){
  Scanner input = new Scanner(System.in);
  //step one is to prompt the user of the three different parameters of a pyramid's volume
  System.out.print("Please input the width of the pyramid in the form xx.xx ");
  double width  = input.nextDouble();
  System.out.print("Please input the length of the pyramid in the form xx.xx ");
  double length = input.nextDouble();
  System.out.print("Please input the height of the pyramid in the form xx.xx ");
  double height = input.nextDouble();
  //calculate and display the volume on the console.
  double pyramidVolume = (int) (width*length*height/3.0*100)/100.0;
  System.out.println("The volume of the pyramid is: "+pyramidVolume);
  }// end of method
}// end of class