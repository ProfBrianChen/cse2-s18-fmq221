/*
Felix Quintana
Prof. Chen
Hw03 2/10/18
TThe objective  of this code is to figure out how much volume in cubic miles
A certain area receives.
*/
import java.util.Scanner;
public class Convert{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: ");
    double acres = input.nextDouble();
    System.out.print("Enter the amount of rain in inches in the form xx.xx ");
    double inchesrain = input.nextDouble();
    double gallons = inchesrain*acres*27154.285990761;//acres inches to gallons converstion
    double cubicmiles = gallons/1.1011e+12;// straight up converstion from gallons to cubicmiles
    //double cubicinches = gallons*26/6006; // 26 gallons per 6006inches cubed
    //double cubicfeet  = cubicinches/Math.pow(12,3);
    //double cubicmiles = cubicfeet/Math.pow(5280,3);
    System.out.println("rain in cubic meters: "+cubicmiles);   
  }//end of main
}//end of class