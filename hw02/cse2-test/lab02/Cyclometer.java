/* Felix Quintana
Prof Chen
lab02
2/2/2018
The program is to determine the time of trips, the distances of trips, and the number of rotations. It was determined by
the inputs of seconds of trips and the diameter for the wheel*/

public class Cyclometer{
    public static void main(String[] agrs)  {
 //  Assigning the identifier is a variable and declaring what kind of variable it is(int) for the first three, and a double for the last.
      
        int secsTrip1=480;  // assigning the identifier is a variable and declaring what kind of variable it is(int)
        int secsTrip2=3220;  //assigning the identifier is a variable and declaring what kind of variable it is(int)
        int countsTrip1=1561;  //assigning the identifier is a variable and declaring what kind of variable it is(int)
        int countsTrip2=9037; //assigning the identifier is a variable and declaring what kind of variable it is(int)
        double wheelDiameter=27.0,  //assigning the identifier is a variable and declaring what kind of variable it is(double)    
        PI=3.14159, //assign the value of the variable
        feetPerMile=5280,  //assigning how many feet for Mile
        inchesPerFoot=12,   //assigning how many feet per foot
        secondsPerMinute=60;  //assigning the value of the variable, which is how many seconds are in a minute
        double distanceTrip1, distanceTrip2,totalDistance;  //assigning the identifier is a variable and declaring what kind of variable it is(double)

  System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");// Printing out how long the trip was in minutes and the counts with combining strings and int type into a single string
  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");// printing out how long the second trip using the same process as the previous line of code

  distanceTrip1=countsTrip1*wheelDiameter*PI;
        // Above gives distance in inches
        //(for each count, a rotation of the wheel travels
        //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;// gives distance in miles
    totalDistance=distanceTrip1+distanceTrip2;// gives the total distance

  //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");// print out the total distance in miles for trip 1
    System.out.println("Trip 2 was "+distanceTrip2+" miles");// print out the total distance in miles for trip 2
    System.out.println("The total distance was "+totalDistance+" miles");// print out the total distance in miles

  
      } //ending the method
}//ending the class