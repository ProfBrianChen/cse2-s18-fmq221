/* Felix Quintana
CSE 002
Professor Chen
1/29/18 */
// Wrote what my class was and the method that I am using. 
//The objective of my code is to calculate the costs of the items inputed before and after sales tax, and the total sales tax, and total bill.

public class Arithmetic{
    public static void main(String[] args){
  
 // declaring my input variables
      //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per box of envelopes
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
//total cost of pants without taxes
double totalcostofpants;
// sales tax of pants
double salestaxofpants;
 
//total cost of shirts without taxes
double totalcostofshirts;
// sales tax of shirts
double salestaxofshirts;

//total cost of belts without taxes
double totalcostofbelts;
// sales tax of belts
double salestaxofbelts;

//total sales tax and total bill before sales tax and total bill respectively
double  totalsalestax;
double totalcostbeforetax;
double totalbill;
 
// calculations of the total cost of each item
totalcostofpants = numPants*pantsPrice;  
totalcostofshirts = numShirts*shirtPrice;
totalcostofbelts = numBelts*beltCost;
 //sales tax of each with adjusted means to get rid of extra decimal points 
salestaxofpants = totalcostofpants*paSalesTax*100;
salestaxofshirts = totalcostofshirts*paSalesTax*100;
salestaxofbelts = totalcostofbelts*paSalesTax*100;
int salestaxofpantsCorrection;      
salestaxofpantsCorrection = (int) salestaxofpants;
 double salestaxofpantsCorrection2;
      salestaxofpantsCorrection2 = salestaxofpantsCorrection / 100.0; // divide so I can get back my original value after cutting it off by declaring it as a int
int salestaxofshirtsCorrection;      
salestaxofshirtsCorrection = (int) salestaxofshirts;
 double salestaxofshirtsCorrection2;
      salestaxofshirtsCorrection2 = salestaxofshirtsCorrection / 100.0;
int salestaxofbeltsCorrection;      
salestaxofbeltsCorrection = (int) salestaxofbelts;
 double salestaxofbeltsCorrection2;
      salestaxofbeltsCorrection2 = salestaxofbeltsCorrection / 100.0;
      
  
      
 //total of sales tax and total of bill before sales tax
totalsalestax = salestaxofpantsCorrection2 + salestaxofshirtsCorrection2 + salestaxofbeltsCorrection2;
totalcostbeforetax = totalcostofpants + totalcostofshirts + totalcostofbelts;
//total bill
totalbill = totalsalestax + totalcostbeforetax;
 
//display the total of each item before tax and then tax of each item total cost before taxes and total sales tax, and display the total bill.
//total cost of pants
System.out.println("Total cost of pants: $"+ totalcostofpants);
// total sales tax of pants
System.out.println("Total sales tax of pants: $"+salestaxofpantsCorrection2);
// total cost of shirts
System.out.println("Total cost of shirts: $"+totalcostofshirts);
//  totalsalestax of shirts
System.out.println("Total sales tax of shirts: $"+salestaxofshirtsCorrection2);
// total cost of belts
System.out.println("Total cost of belts: $"+totalcostofbelts);
//  totalsalestax of belts
System.out.println("Total sales tax of belts: $"+salestaxofbeltsCorrection2);
 // total cost 
System.out.println("Total cost: "+ totalcostbeforetax);
//  totalsalestax
System.out.println("Total sales tax: "+totalsalestax);
// total bill
 System.out.printf("Total bill: %.2f",totalbill);
  
  
  
    } //end of method
} //end of class