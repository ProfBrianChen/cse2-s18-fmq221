Grade: 98. 
Compiles and runs without error.
-2 for formatting of curly braces and indentations. Everything inside of the main method should be indented only once in this case. See textbook for proper formatting. 
This isn't a major issue, but practicing good habits is beneficial because proper formatting makes your code easier to read. 
Remember to use units when applicable (in this case, dollars) (no points deducted this time).
Great use of comments!