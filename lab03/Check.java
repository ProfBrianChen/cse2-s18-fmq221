/*
Felix Quintana
Prof Chen
Lab03
*/



import java.util.Scanner;
// The objective of this program is to calculate the total cost of each individual in a party after including a tip. 
//
//
public class Check{
                // main method required for every Java program
               public static void main(String[] args) {
              Scanner myScanner = new Scanner( System.in);
              System.out.print("Enter the original cost of the check in the form xx.xx: ");// print out the statement and not continue until done so.
              double checkCost = myScanner.nextDouble(); // input the value above
              System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
              double tipPercent = myScanner.nextDouble(); // input value above
              tipPercent /= 100; //We want to convert the percentage into a decimal value
              System.out.print("Enter the number of people who went out to dinner: ");
              int numPeople = myScanner.nextInt();
              double totalCost;
double costPerPerson;
int dollars,   //whole dollar amount of cost 
     dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;  // returning the remainder after dividing by 10
pennies=(int)(costPerPerson * 100) % 10; // same process
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); // how much everyone owes

               
               
               
               
         }  //end of main method   
 } //end of class

