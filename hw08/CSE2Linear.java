//Felix Quintana
// CSE2 Hw08
//Objecives:The objective of this code is to alow the user to input 15 integers into an array and make sure the user inputs integers
//Futhermore it make sures it increases.
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
  public static void main(String[] args){
    boolean trulyInt = true;
    int grade = 0;
    boolean acension = true;
    Scanner input = new Scanner(System.in);
    System.out.println("Please input fifteen integers of the students grades:");
    int[] StudentGrade = new int[15];
    for(int i = 0; i< 15; i++){
      do{
        acension = true;
        //this loop makes sure that the user inputs a integer and if it is in the bounds.
        do{
           trulyInt = input.hasNextInt();
           if( trulyInt == true){
             grade = input.nextInt();
             if( grade < 0 || grade > 100){
               System.out.print("Sorry out of bounds. Try again: ");
               trulyInt = false;
             }
           }
           else{
             System.out.print("Sorry wrong input. Try again: ");
             String junk = input.next();
           } 
        }while( trulyInt == false);
        //if it passes through the loop store the integer as the ith element of the array.
        // this for loop checks that value is greater than all the other values previous.
        StudentGrade[i] = grade;
        if( i != 0){
          for( int j = 0; j <= i; j++){
            if( StudentGrade[i] < StudentGrade[j]){
              System.out.print("Sorry, that grade is lower than the previous grade: ");
              acension = false;
              break;
            }
          }
        }  
      }while( acension == false);
    }
    //prints the array.
      for(int k = 0; k < 15; k++){
        System.out.print(StudentGrade[k] + "  ");
        if( k ==14){
          System.out.println();
        }
      }
    //thia is where I use my function to search it in a binary way.
    System.out.print("Please enter a grade to search for: ");
    int Search = input.nextInt();
    binary(Search,StudentGrade);
    //now this is where I utilize my linear search.
    System.out.println("Scrambled: ");
    int newarray[] = Scrambled(StudentGrade);
    System.out.print("Enter a value to be searched: ");
    int targetVal = input.nextInt();
    linearSearch(newarray,targetVal);
  }
     
  public static void binary(int Search, int[] StudentGrade){
    if ( Search > StudentGrade[14] || Search < StudentGrade[0]){
        System.out.println("There is no " + Search + " in the array from one interation. It's outside the bounds!");
    }
    int min = 0;
    int max = 14;
    int middle = (max + min)/2;
    int counter=0;
    
    //this is where as long as I have not found my searched value; 
    while( Search !=StudentGrade[middle]){
      if( Search > StudentGrade[middle]){
        min = middle;
      }
      else if ( Search < StudentGrade[middle]){
        max = middle;
      }
      middle = (max + min)/2;
      System.out.print(max);
      System.out.println(min);
      //this is how I know if the value couldn't be found.
      if( max == min + 1){
        break;
      }
      counter++;        
    }
    //display either option depending if I found the value or not.
    if ( Search !=StudentGrade[middle]){
      System.out.println(Search + " was not found with "+ counter + " iterations");
    }
    else{
      System.out.println(Search + " was found with " + counter + " iterations");
    }
  }
  // This is my code that changes the order.
  public static int[] Scrambled(int[] StudentGrade){
    Random rand = new Random();
    for (int i=0; i<StudentGrade.length; i++) {
    //find a random member to swap with
    int target = (int) (StudentGrade.length * Math.random() );

    //swap the values
    int temp = StudentGrade[target];
    StudentGrade[target] = StudentGrade[i];
    StudentGrade[i] = temp;
    
}
    //print out the new array.
    for(int j = 0; j< StudentGrade.length; j++){
      System.out.print(StudentGrade[j]+ " ");
    }     
     return StudentGrade;
    }  
  //this is my linear search where I make a for loop to check each value and add to counter if it is at all
    public static void linearSearch( int[] array, int target){
      int counter=0;
      for( int i = 0; i< array.length; i++){
        if ( array[i] == target){
          counter++;
        }
      }
      //prints out how many times it was found or not found,
      if( counter > 0){
        System.out.print(target + " was found " + counter +" times in one iteration");
      }
      else{
         System.out.print(target + " was not found in one iteration");
      }
    }
  }
