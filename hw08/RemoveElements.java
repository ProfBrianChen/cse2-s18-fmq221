//Felix Quintana
//CSE2
//Prof Chen
//Objectives: To use three different methods to delete a certain psotion in a array or delete elements in array that has a certain value.
import java.util.Random;
import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    // random ten integers are generated and then put into a function to delete what location of the array that the user decides.
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      // they enter the target value to be what is deleted in the array.
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
       
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  //creates an array and then it creates a random number from 0-10 and fills thsoe values in the array.
  public static int[] randomInput(){
    Random rand = new Random();
    int firstarray[] = new int[10];
    int randomnum;
    for( int k = 0; k < 10; k++ ){
      randomnum = rand.nextInt(10);
      firstarray[k] = randomnum;
    }
    return firstarray;
  }
  // this function starts with a loop to find the array  value that has the target value. and then doesn't write that value onto the new array.
  public static int[] delete(int num[], int index){
    int num2[] = new int[9];
    int counter = 0;
    for( int i = 0; i < num.length; i++){
      if ( i == index){
        counter++;
        continue;
      }
      else{
      num2[i - counter] = num[i];
      }
    }
    
    return num2;
  }
  // this one counts how many times the previous array has the target value and counts it. it then usses that counter to decide how long the array is.
  public static int[] remove(int num[], int target){
    int counter = 0;
    for( int i = 0; i < num.length; i++){
      if (num[i] == target){
        counter++;
        continue;
      }
    }
    //this is where I write the new array skipping when the old array is equal to target
    int num2[] = new int[num.length - counter];
    int counter2 = 0;
    for( int k = 0;  k < num.length; k++){
      if ( target == num[k]){
        counter2++;
        continue;
      }
      num2[k-counter2] = num[k];
    }
    return num2;
  }
}
