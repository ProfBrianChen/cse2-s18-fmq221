// Felix Quintana
// lab08

import java.util.Scanner;
import java.util.Random;
public class lab08{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    Random rand = new Random();
    int randomInt = rand.nextInt(6) + 5;
    
    String[] Students = new String[randomInt];
    System.out.println("Enter the "+ randomInt +" names");
    for (int i = 0; i < randomInt; i++){
      Students[i] = input.next();
    }
     int[] Studentsgrades = new int[randomInt];
    for( int k = 0; k< randomInt; k++){
      Studentsgrades[k]= rand.nextInt(99)+1;
    }
    for(int j = 0; j< randomInt; j++){
      System.out.println(Students[j] + ": "+Studentsgrades[j]);
    }
  }
}