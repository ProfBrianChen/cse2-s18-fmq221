/*
Felix Quintana
Prof. Chen
CSE002
Lab04
Objective: To write a program that chooses and displays a random card that I pick showing suit and card number
*/
public class CardGenerator{
  public static void main(String[] args){
    // generate a number for myself
    int RandomNum = (int) (Math.random()*52);
    String suit = "Sup";
    // deciding what suit it is in
    if ( RandomNum <= 13){
      suit = "Diamonds"; 
    }
    else if( RandomNum <= 26 ){
      suit = "Clubs";
    }
    else if ( RandomNum <= 39){
      suit = "Hearts";
    }
    else if ( RandomNum <= 52){
      suit = "Spades";
    }
    
    //finding the card num by the remainder
    switch( RandomNum % 13){
      case 0 : System.out.println("You picked King of" + suit);
        break;
      case 1 : System.out.println("You picked Ace of " + suit);
        break;
      case 2 : System.out.println("You picked two of " + suit);
        break;
      case 3 : System.out.println("You picked three of " + suit);
        break;
      case 4 : System.out.println("You picked four of " + suit);
        break;
      case 5 : System.out.println("You picked five of " + suit);
        break;
      case 6 : System.out.println("You picked six of " + suit);
        break;
      case 7 : System.out.println("You picked seven of " + suit);
        break;
      case 8 : System.out.println("You picked eight of " + suit);
        break;
      case 9 : System.out.println("You picked nine of " + suit);
        break;
      case 10 : System.out.println("You picked ten of " + suit);
        break;
      case 11 : System.out.println("You picked Jack of " + suit);
        break;
      case 12 : System.out.println("You picked Queen of " + suit);
      
   
   }
    
  } // end of main method
}  // end of class