//Felix Quintana
//Prof. Chen
//Objectives: The objective of this code is to create a two dimensional array that consists of value bounded integers.
//I then print out the array that I constructed. I then use a function called update to randomly assign members a negative value.
//Finally, I make a function to find those negative values and make them positive, and In turn make the following member in the member negative and display the outcome.
import java.util.Random;
public class RobotCity{
  public static void main(String[] args){
    Random rand = new Random(); 
    /*Set up so I can make a random integer of bots that will attack the city.
    I start of by every loop making a new random numofRobots. I then in each iteration run through several functions.
    The first function being to create my two dimensional array to be used futher on.
    I follow up to have a function to print out my array in a row major format.There after, I run invade to make values in my array negative. It runs in random order, and how many are produced
    are dependent on the value that was randomly generated numofRobots. Next, I create a function called update that
    does the part where it makes the negative values previously positive, and then makes the next member of that member array negative.
    */
    for(int i = 0; i < 5; i++){
      int randNumOfBots = rand.nextInt(5)+1;
      int City[][] =buildCity();   
      display(City);
      invade(City,randNumOfBots);
      display(City);
      update(City);
      display(City);
    }
  }
  public static int[][] buildCity(){
    Random rand = new Random();
    int randomPop = 0;
    int randomCityColumns = rand.nextInt(6) + 10;
    int randomCityBlocks  = rand.nextInt(6) + 10;
    int builtCity[][] = new int[randomCityColumns][randomCityBlocks];
    for(int i = 0; i < builtCity.length;  i++){
      for( int j = 0; j < builtCity[i].length; j++){
        randomPop = rand.nextInt(899) + 100;                       //This is where that member gets a random value from 100 to 999.
        builtCity[i][j] = randomPop;
      }
    }
    return builtCity;
  }
  public static void display(int theCity[][]){
    for(int k = 0; k < theCity.length; k++){
      for(int j = 0; j < theCity[k].length; j++){
        System.out.format(" %d", theCity[k][j]);    //Prints out the value of the member array at k and member j.
      }
      System.out.println();
    }
    System.out.println();
    System.out.println();
  }
  public static int[][] invade(int city[][], int numofRobots){
    Random rand = new Random();
    int randomCityColumns = 0;
    int randomCityBlocks  = 0;
    int temp[] = new int[numofRobots];
    boolean legitmacy = true;
    for( int k= 0; k < numofRobots; k++){
      do{ 
        legitmacy = true;
        randomCityColumns = rand.nextInt(city.length-1);                        //The point of this code is to insure that the randomly chosen value isn't choosen twice.
        randomCityBlocks  = rand.nextInt(city[randomCityColumns].length - 1);   //However there is the small chance that the two randomly chosen arrays have the same exact value. Fortunately those chances are extremely slim.
        temp[k] = city[randomCityColumns][randomCityBlocks]*-1;                 //The do while loop is meant to catch these usual conditions except the one I specifically mentioned a moment ago.
        city[randomCityColumns][randomCityBlocks] = temp[k];                    //I'd use a catch try block if I knew how to do it :(
         if(k != 0){
           for( int i = 0; i < k; i++){
             if( temp[k] == temp[k-i-1]){
               legitmacy = false;
             }
           }
         }
      }while(legitmacy == false);
    }
    return city;
  } 
  public static void update(int city[][]){
    int tempy  =  0;
    int tempy2 =  0;
    for(int i =0; i<city.length; i++){
      for(int k = city[i].length-1; k >= 0 ; k--){
        if( city[i][k] < 0){
          tempy = city[i][k]*-1;
          city[i][k] = tempy;          
         if(k != city.length -1){            //This checks I won't get a error bounds when I am making the next member negative. If I am at the ened of the member array, I will not attempt to make the next one negative that is nonexistant. 
            tempy2 = city[i][k+1]*-1;
            city[i][k+1] = tempy2;
          }
        }
      }
    }
  }
}