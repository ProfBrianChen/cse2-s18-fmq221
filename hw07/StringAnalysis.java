/* Felix Quintana
Prof Chen.
Objectives: The objective for this code is to determine if the users string contains all letters. It is done in two ways. One is to check all the letters in the string, or check only up only to a certain value the user inputs.
*/
import java.util.Scanner;
public class StringAnalysis{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    int UsersChoiceofCharacters;
    boolean trulyAllLetters = true;
    boolean UserPreference;
    System.out.print("Please input a string of characters to check: ");
    String UserInput = input.next();
    System.out.print("Would you like to check the entire string please enter 0. However, if so, enter the value as a integer");
/*The idea behind the following loop was to cycle again if it was not a integer. If it was a integer it would store it as the amount of placement in the string.
However, if the integer was zero then that would be interpreted as the entire string. Also, if it was les than zero it would make the loop start again just like if it was not a integer.
In the repective sections I put the appropriate method to be calculated using the inputed string and the placement of the string if the integer was greater than zero. Afterwards, the resulting
boolean would be my condition to determine if it is all letter or not.*/
    
    do{ 
      UserPreference = input.hasNextInt();
      if( UserPreference == true){
        UsersChoiceofCharacters = input.nextInt();
        if( UsersChoiceofCharacters == 0){
          trulyAllLetters = checkcharacters(UserInput);
        }
        else if( UsersChoiceofCharacters > 0){
          trulyAllLetters = checkcharacters(UserInput, UsersChoiceofCharacters);
        }
        else{
          System.out.print("Sorry that was not the correct input: ");
           UserPreference= false;
        }
      }
      else{
        System.out.print("Sorry that was not the correct input: ");
        String juinkstuff = input.next();
      }
    }while(UserPreference == false);
    
    
    if(trulyAllLetters == true)
      System.out.println("Congrats, they are all letters!Wooo!(Enthusiasm)");
    else
      System.out.println("Sorry, they are not all letters.");
  }
  /*These to methods were what I used for finding if the string is letters up to a certain point, and the entire string respectively. I used UserInput.length() to determine
  how long the string was, and for the first function it would break out of the loop if the i value was greater than the length of the string a.k.a UserInput.length().*/
  
  
  public static boolean checkcharacters( String UserInput, int charactercount){
    boolean ifLetter = true;
    for( int i = 0; i < charactercount; i++ ){
      if( i > UserInput.length() ){
        break;
      }
      char UsersChar = UserInput.charAt( i );
      ifLetter = Character.isLetter(UsersChar);
       if( ifLetter == false){
        break;
      }
      ifLetter = true;
    }
    return ifLetter;
  }
  public static boolean checkcharacters( String UserInput){
    boolean ifLetter = true;
    for(int i = 0; i < UserInput.length(); i++){
      char UsersChar =UserInput.charAt( i );
      ifLetter = Character.isLetter(UsersChar);
      if( ifLetter == false){
        break;
      }
      ifLetter = true;
    }
    return ifLetter;
  }
}