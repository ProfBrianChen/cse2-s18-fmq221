/* Felix Quintana
Prof Chen.
Objectives: The objective of this program is to use four additional methods besides the main method to calculate the area of three different shapes: circle, rectangle, triangle.
//////////Futhermore, I check if the user inputs a double by calling a function to take the input of the user and after checking if it is a integer I store that value and return it.
//////////Lastly, after I return the value I use another function with the input of the function being my return of the function checking if it is a double. The second function serves
/////////to calculate the areas.
*/
import java.util.Scanner;
public class Area{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    boolean leaveLoop = true;
    
    //Each block of code is meant to check if the user inputed a double by the function checkDouble and then use its return for the function to calculate the area. The two need to check and have
    //more than one double.
    
    do{
      System.out.print("If you would like to calulate the area of a rectangle, triangle, circle please type them respective ");
      String response = input.nextLine();
      if("circle".equals(response)){
        System.out.print("Please input a radius as type double: ");
        double radius = checkDouble();
        double areaCircle = areaofCircle(radius);
        System.out.println("The area of the circle is: " + areaCircle);
      }
      else if("rectangle".equals(response)){
        System.out.print("Please input the length of the rectangle: ");
        double heightofRect = checkDouble();
        System.out.print("Please input the width of the rectangle: ");
        double widthofRect = checkDouble();
        double areaCircle   = areaofRectangle(heightofRect,heightofRect);
        System.out.println("The area of the rectangle is: " + areaCircle);
      }
      
      else if("triangle".equals(response)){
        System.out.print("Please input the length of the triangle: ");
        double heightofTri = checkDouble();
        System.out.print("Please input the width of the triangle: ");
        double widthofTri = checkDouble();
        double areaTriangle   = areaofTriangle(heightofTri,widthofTri);
        System.out.println("The area of the rectangle is: " + areaTriangle);
      }
      else{
        System.out.println("Sorry that shape was not one of the allowed shapes");
        leaveLoop = false;
      }
    }while(leaveLoop == false);
  }
  //This method again is what checks for if what is inputed is a double. If not than stay in a loop until it is and store it.
  
  public static double checkDouble(){
    Scanner input = new Scanner(System.in);
    double radius = 0;
    boolean possibleDouble;
    boolean possibleInt = false;
    do{
      possibleDouble = input.hasNextDouble();
      possibleInt = input.hasNextInt();
      if( possibleDouble == true && possibleInt == false){
        radius = input.nextDouble();
        if( radius < 0){
          System.out.print("Sorry, thats a negative number. Please try again: ");
          possibleDouble = false;
        }
      }
      else{
        System.out.print("Sorry, that's not a double please try again");
        String junk = input.next();
        possibleDouble = false;
      }
    }while( possibleDouble == false); 

    return radius;
  }
  //Calculating each area respectively.
  public static double areaofCircle( double radius ){
    double area = Math.PI*radius*radius;
    return area;
  }
  public static double areaofRectangle( double width, double height){
    double area = width*height;
    return area;
  }
  public static double areaofTriangle( double width, double height ){
    double area = width*height/2.0;
    return area;
  }
}  