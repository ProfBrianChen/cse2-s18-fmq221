/*
Felix Quintana
Prof. Chen
Hw04
Objective: The goal of the program is to make a version of Yahtzee with calculating an upper and lower score and a grand score with either random generated dice or choosen numbers.
yahtzee.java
*/
import java.util.Scanner;
public class Yahtzee{
  public static void main(String[] agrs){
    Scanner input = new Scanner(System.in);
    // Needed initializations for later calculations
    int UpperTotal   = 0;  // Can you let me know if this is a good way to set this up please. I want to write my code as profession and easy to read as possible. Thank you.
    int myRollDice1  = 0;
    int myRollDice2  = 0;
    int myRollDice3  = 0;
    int myRollDice4  = 0;
    int myRollDice5  = 0;
    int Aces         = 0;
    int Twos         = 0;
    int Threes       = 0;
    int Fours        = 0;
    int Fives        = 0;
    int Sixes        = 0;
    int RepeatofDice = 0;
    int pair         = 0;
    int TwoPairs     = 0;
    int DicePair     = 0;
    int ThreeKind    = 0; // There is Threekind and Fourkind, awefully close to the other two variables ThreeofKind and Fourkind. I did this so I could sum up each loop.
    int Yahtzee      = 0;
    int Fourkind     = 0;
    int FullHouse    = 0;
    int SmallStraight= 0;
    int LargeStraight= 0; 
    int Chance       = 0;
  
    //Let the user decide if he or she would like to
    //Choose to random roll or choose the digits.
    // for loop so it allows me to run the code 6 times that i need so it can run through a full game of yahtzee that was shown on the hw.
    for( int i=1; i<=6; ){   
      System.out.println("If you would like to roll randomly please type the number 1,");
      System.out.print("Or if you would like to choose the numbers type 2 ");
      int ThreeofKind    = 0;
      int FourKind       = 0;
      int myChoice = input.nextInt();
      // this excutes the choice of having a random number generator to decide the roll
      if ( myChoice == 1){
        myRollDice1 = (int)(Math.random()*6 + 1);
        myRollDice2 = (int)(Math.random()*6 + 1);
        myRollDice3 = (int)(Math.random()*6 + 1);
        myRollDice4 = (int)(Math.random()*6 + 1);
        myRollDice5 = (int)(Math.random()*6 + 1);
      }
    //The roll that the user chooses
      else if( myChoice == 2){
        System.out.print("Please input a digit from 1 to 6 of what your roll will be: ");
        myRollDice1 = input.nextInt();
        System.out.print("Please input a digit from 1 to 6 of what your roll will be: ");
        myRollDice2 = input.nextInt();
        System.out.print("Please input a digit from 1 to 6 of what your roll will be: ");
        myRollDice3 = input.nextInt();
        System.out.print("Please input a digit from 1 to 6 of what your roll will be: ");
        myRollDice4 = input.nextInt();
        System.out.print("Please input a digit from 1 to 6 of what your roll will be: ");
        myRollDice5 = input.nextInt();
        // Checking if each dice they chooose is between 1 and 6
        boolean checkRollChoice1 = ( myRollDice1 > 0 && myRollDice1 <= 6);
        if (checkRollChoice1 == false){
          System.out.println("Sorry " + myRollDice1 +" is not a choice.");
          System.exit(0);
        }   
        boolean checkRollChoice2 = ( myRollDice2 > 0 && myRollDice2 <= 6);
        if (checkRollChoice2 == false){
          System.out.println("Sorry " + myRollDice2 +" is not a choice.");
          System.exit(0);
        }   
        boolean checkRollChoice3 = ( myRollDice3 > 0 && myRollDice3 <= 6);
        if (checkRollChoice3 == false){
          System.out.println("Sorry " + myRollDice3 +" is not a choice.");
          System.exit(0);
        }   
        boolean checkRollChoice4 = ( myRollDice4 > 0 && myRollDice4 <= 6);
        if (checkRollChoice4 == false){
          System.out.println("Sorry " + myRollDice4 +" is not a choice."); 
          System.exit(0);
        }   
        boolean checkRollChoice5 = ( myRollDice5 > 0 && myRollDice5 <= 6);
        if (checkRollChoice5 == false){
          System.out.println("Sorry " + myRollDice5 +" is not a choice."); 
          System.exit(0);
        }   
      }
      else{
        System.out.println("Sorry, not the right type of response.");
      }
      
    // Now its time to calculate the upper score  
      //  this adds up all the ones twos etc in all the rolls
      switch( myRollDice1){
        case 1 : Aces = Aces + myRollDice1; break;
        case 2 : Twos = Twos + myRollDice1;  break;
        case 3 : Threes = Threes + myRollDice1; break;
        case 4 : Fours = Fours + myRollDice1; break;
        case 5 : Fives = Fives + myRollDice1; break;
        case 6 : Sixes = Sixes + myRollDice1; break;
      }
      switch( myRollDice2){
        case 1 : Aces = Aces + myRollDice2; break;
        case 2 : Twos = Twos + myRollDice2; break;
        case 3 : Threes = Threes + myRollDice2; break;
        case 4 : Fours = Fours + myRollDice2; break;
        case 5 : Fives = Fives + myRollDice2; break;
        case 6 : Sixes = Sixes + myRollDice2; break;
      } 
      switch( myRollDice3){
        case 1 : Aces = Aces + myRollDice3; break;
        case 2 : Twos = Twos + myRollDice3; break;
        case 3 : Threes = Threes + myRollDice3; break;
        case 4 : Fours = Fours + myRollDice3; break;
        case 5 : Fives = Fives + myRollDice3; break;
        case 6 : Sixes = Sixes + myRollDice3; break;
      }
      switch( myRollDice4){
        case 1 : Aces = Aces + myRollDice4;     break;
        case 2 : Twos = Twos + myRollDice4;     break;
        case 3 : Threes = Threes + myRollDice4; break;
        case 4 : Fours = Fours + myRollDice4;   break;
        case 5 : Fives = Fives + myRollDice4;   break;
        case 6 : Sixes = Sixes + myRollDice4;  break;
      }
      switch( myRollDice5){
        case 1 : Aces = Aces + myRollDice5;     break;
        case 2 : Twos = Twos + myRollDice5;     break;
        case 3 : Threes = Threes + myRollDice5; break;
        case 4 : Fours = Fours + myRollDice5;   break;
        case 5 : Fives = Fives + myRollDice5;   break;
        case 6 : Sixes = Sixes + myRollDice5;  break;
      }
      // This is now the start for my calculation of my lower score. I first check if all the dice are all the same. I am counting on how many repeats of dice numbers so than I can figure out the things I need like three of the same etc.
      // So i check every possibility of combinations of the five dice. As well as if there could be a pair with combinations so I can figure out if its a full house or just a pair etc.
      // Note that some of the variables I assign values to are not what is needed for any of the actual calulation, but I used them to help debug my program. 
      boolean repeat = myRollDice1 == myRollDice2 && myRollDice1 == myRollDice3 && myRollDice1 == myRollDice4 && myRollDice1 == myRollDice5;  
      if ( repeat == true ){
        RepeatofDice = 5;
        pair = 0;
      }
      else if( repeat == false ){
        if( myRollDice1 == myRollDice2 && myRollDice1 == myRollDice3 && myRollDice1 == myRollDice4){
          RepeatofDice = 4;
          FourKind = myRollDice1 + myRollDice2 + myRollDice3 + myRollDice4;
        }
        else if(myRollDice1 == myRollDice2 && myRollDice1 == myRollDice3 && myRollDice1 == myRollDice5){
          RepeatofDice = 4;
          FourKind = myRollDice1 + myRollDice2 + myRollDice3 + myRollDice5;
        }
        else if ( myRollDice1 == myRollDice2 && myRollDice1 == myRollDice4 && myRollDice1 == myRollDice5){
          RepeatofDice = 4;
          FourKind = myRollDice1 + myRollDice2 + myRollDice4 + myRollDice5;
        }
        else if ( myRollDice1 == myRollDice3 && myRollDice1 == myRollDice4 && myRollDice1 == myRollDice5){
          RepeatofDice = 4;
          FourKind = myRollDice1 + myRollDice3 + myRollDice4 + myRollDice5;
        }
        else if( myRollDice2 == myRollDice3 && myRollDice2 == myRollDice4 && myRollDice2 == myRollDice5){
          RepeatofDice = 4;
          FourKind = myRollDice2 + myRollDice3 + myRollDice4 + myRollDice5;
        }
        else if( myRollDice1 == myRollDice2 && myRollDice1 == myRollDice3){
          RepeatofDice = 3;
          ThreeofKind = myRollDice1 + myRollDice2 + myRollDice3;
          if( myRollDice4 == myRollDice5 ){
            pair = 1;
            ThreeofKind = 0;
          }  
        }
        else if( myRollDice1 == myRollDice2 && myRollDice1 == myRollDice4){
          RepeatofDice = 3;
          ThreeofKind = myRollDice1 + myRollDice2 + myRollDice4;
         if( myRollDice3 == myRollDice5 ){
           pair = 1;
           ThreeofKind = 0;
         } 
        }
        else if( myRollDice1 == myRollDice2 && myRollDice1 == myRollDice5){
          RepeatofDice = 3;
          ThreeofKind = myRollDice1 + myRollDice2 + myRollDice5;
          if( myRollDice3 == myRollDice4 ){
            pair = 1;
            ThreeofKind = 0;
         }
        }
        else if( myRollDice1 == myRollDice3 && myRollDice1 == myRollDice4){
          RepeatofDice = 3;
          ThreeofKind = myRollDice1 + myRollDice3 + myRollDice4;
          if( myRollDice2 == myRollDice5 ){
            pair = 1;
            ThreeofKind = 0;
         }  
        }
        else if( myRollDice1 == myRollDice3 && myRollDice1 == myRollDice5){
          RepeatofDice = 3;
          ThreeofKind = myRollDice1 + myRollDice3 + myRollDice5;
          if( myRollDice2 == myRollDice4 ){
            pair = 1;
            ThreeofKind = 0;
         }      
        }
        else if( myRollDice1 == myRollDice4 && myRollDice1 == myRollDice5){
          RepeatofDice = 3;
          ThreeofKind = myRollDice1 + myRollDice4 + myRollDice5;
          if( myRollDice2 == myRollDice3 ){
            pair = 1;
            ThreeofKind = 0;
         }
        }
        else if ( myRollDice2 == myRollDice3 && myRollDice2 == myRollDice4){
          RepeatofDice = 3;
          ThreeofKind = myRollDice2 + myRollDice3 + myRollDice4;
          if( myRollDice1 == myRollDice5 ){
            pair = 1;
            ThreeofKind = 0;
         }  
        }
        else if( myRollDice2 == myRollDice3 && myRollDice2 == myRollDice5){
          RepeatofDice = 3;
          ThreeofKind = myRollDice2 + myRollDice3 + myRollDice5;
          if( myRollDice1 == myRollDice4 ){
            pair = 1;
            ThreeofKind = 0;
         }
        }
        else if( myRollDice2 == myRollDice4 && myRollDice2 == myRollDice5){
          RepeatofDice = 3;
          ThreeofKind = myRollDice2 + myRollDice4 + myRollDice5;
          if( myRollDice1 == myRollDice3 ){
            pair = 1;
            ThreeofKind = 0;
          }
        }
        else if( myRollDice3 == myRollDice4 && myRollDice3 == myRollDice5){
          RepeatofDice = 3; 
          ThreeofKind = myRollDice3 + myRollDice4 + myRollDice5;
          if( myRollDice1 == myRollDice2 ){
            pair = 1;
            ThreeofKind = 0;
          }
        }
        else if( ((myRollDice1 == myRollDice2)) || (( myRollDice1 == myRollDice3)) || (myRollDice1 ==  myRollDice4) || (myRollDice1 == myRollDice5)){
          RepeatofDice = 2;
          if( (myRollDice2 == myRollDice3) || (myRollDice2 == myRollDice4) || (myRollDice2 == myRollDice5)){
            pair = 2;
          }
          else if( (myRollDice3 == myRollDice4) || (myRollDice4 == myRollDice5) || (myRollDice3 == myRollDice5)){
                pair = 2;
          }
        }  
        else if( (myRollDice2 == myRollDice3) || (myRollDice2 == myRollDice4) || (myRollDice2 == myRollDice5)){
          RepeatofDice = 2;
          if( ((myRollDice1 == myRollDice2)) || (( myRollDice1 == myRollDice3)) || (myRollDice1 ==  myRollDice4) || (myRollDice1 == myRollDice5)){
            pair = 2;
          }
          else if( (myRollDice3 == myRollDice4) || (myRollDice4 == myRollDice5) || (myRollDice3 == myRollDice5)){
            pair =2;
          }
        }  
        else if( (myRollDice3 == myRollDice4) || (myRollDice4 == myRollDice5) || (myRollDice3 == myRollDice5)){
          RepeatofDice = 2;
          if( (myRollDice2 == myRollDice3) || (myRollDice2 == myRollDice4) || (myRollDice2 == myRollDice5)){
            pair =2;
          }
          else if( ((myRollDice1 == myRollDice2)) || (( myRollDice1 == myRollDice3)) || (myRollDice1 ==  myRollDice4) || (myRollDice1 == myRollDice5)){
            pair = 2;
          }
        }
      }
      else{
        RepeatofDice = 0;
        pair = 0;
      }
      // So now the counting is over, I decide what the amount of repeated values mean for what.
      switch( RepeatofDice){
        case 2 : if( pair == 2){
                   TwoPairs= TwoPairs + 1;
                 }
                 else{
                   DicePair = DicePair + 1;
                 }
                break;
        case 3 : if( pair == 1){
                   FullHouse = FullHouse + 1;
                 }
                 else{
                 ThreeofKind = ThreeofKind;
                 }
                break;  
        case 4 : FourKind = FourKind; break;
        case 5 : Yahtzee = Yahtzee + 1; break;
        case 0 : 
          // I nested my if else if statements in my switch just becuase it was convient, I have to consider when I have a partern note just repeated numbers.
          // These booleans are what I use to literally define the other possible combinations that can get me points
          boolean order1 = myRollDice1 == 1 && myRollDice2 == 2 && myRollDice3 == 3 && myRollDice4 == 4;
          boolean order2 = myRollDice2 ==1 && myRollDice3 == 2 && myRollDice4 == 3 && myRollDice5 == 4;
          boolean order3 = myRollDice1 == 2 && myRollDice2 == 3 && myRollDice3 == 4 && myRollDice4 == 5;
          boolean order4 = myRollDice2 ==2 && myRollDice3 == 3 && myRollDice4 == 4 && myRollDice5 == 5;
          boolean order5 = myRollDice1 == 3 && myRollDice2 == 4 && myRollDice3 == 5 && myRollDice4 == 6;
          boolean order6 = myRollDice2 ==3 && myRollDice3 == 4 && myRollDice4 == 5 && myRollDice5 == 6;
          boolean order7 = myRollDice1 == 1 && myRollDice2 == 2 && myRollDice3 == 3 && myRollDice4 == 4 && myRollDice5 == 5;
          boolean order8 = myRollDice2 ==1 && myRollDice3 == 2 && myRollDice3 == 3 && myRollDice4 == 5 && myRollDice5 == 6;
          if( order7 == true){
            LargeStraight = LargeStraight + 1;
          }   
          else if( order8 == true){
            LargeStraight = LargeStraight + 1;
          } 
          else if( order1 == true){
            SmallStraight = SmallStraight + 1;
          }
          else if( order2 == true){
            SmallStraight = SmallStraight + 1;
          }
          else if( order3 == true){
            SmallStraight = SmallStraight + 1;
          } 
          else if( order4 == true){
            SmallStraight = SmallStraight + 1;
          }
          else if( order5 == true){
            SmallStraight = SmallStraight + 1;
          }
          else if( order6 == true){
            SmallStraight = SmallStraight + 1;
          }
      }
      ThreeKind = ThreeKind + ThreeofKind; // This is what I was talking about summing in the loop.
      Fourkind = Fourkind + FourKind;
      Chance = Chance + myRollDice1 + myRollDice2 + myRollDice3 + myRollDice4 + myRollDice5;
      i = i +1; // increment my loop
    // So the next block comment is meant for me for debugging and seeing if my values are what they are supposed to be. It's quite useful, and I wasn't sure if you'd like to see this or not so, here you go.
      UpperTotal = Aces + Twos + Threes + Fours + Fives + Sixes;
      System.out.println("Repeats: " + RepeatofDice);
      System.out.println("Pairs: " + pair);
      System.out.println("Value of small straight "+SmallStraight);
      System.out.println("Value of Two pairs "+TwoPairs);
      System.out.println("Value of ThreeofKind "+ThreeKind);
      System.out.println("Value of FourKind "+Fourkind);
      System.out.println("Value of FullHouse "+FullHouse);
      System.out.println("Value of Yahtzee "+Yahtzee);
      System.out.println("Value of DicePair "+DicePair);
      System.out.println("Value of LargeStraight"+LargeStraight);
      System.out.println("Value of chance: " + Chance);
     
     pair = 0;
      RepeatofDice =0;     
    }  
    // my totals
    UpperTotal = (UpperTotal < 63) ? UpperTotal : UpperTotal + 35;
    int LowerTotal = ThreeKind + Fourkind + FullHouse*25 +SmallStraight*30 + LargeStraight*40 + Yahtzee*50 + Chance;
    int GrandTotal = UpperTotal + LowerTotal;
    System.out.println("Grand Total" + GrandTotal);
    
        
  }//end of main method
}//end of class