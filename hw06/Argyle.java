/*
Felix Quintana
Prof. Chen
Hw06
The objective of this code is to create a partern of diamonds by loops with the user inputting all the different possibilities
*/
import java.util.Scanner;
public class Argyle{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
   // I start of initializng my variables because they are used in the do while loops.
    int widthofWindow  = 0;                       
    int height         = 0;
    int widthofArgyleD = 0;
    int widthofStripe  =-2;      
    int odd            = 0;
    boolean trulyInt;
    //Asking for the positive integer for the width
    System.out.print("please enter a positive integer for the width of viewing window in characters: ");
    do{
      trulyInt = input.hasNextInt(); 
      if( trulyInt == true){
        widthofWindow = input.nextInt();
     //Checking if the widthofWindow is greater than 0, and if so it'll continue to the end of the loop.
     //If it's not greater than zero it'll overwrite trulyInt as false to make it run the loop again.  
        if( widthofWindow < 0){
         System.out.print("Sorry, please make sure you have a positive integer: ");
         trulyInt = false;
        }
      }
      else{
        System.out.print("Sorry, please make sure you have a positive integer: ");
      String junk = input.next();
      }
    }while( !trulyInt );
    //Asking for a positive integer for the height of the window
    
    System.out.print("please enter a positive integer for the height of viewing window in characters: ");
    do{
      trulyInt = input.hasNextInt();
    //Like the previous loop I am checking if its a integer and if its positive, and if not then make trulyInt false so it can do the loop again.
      if( trulyInt == true){
        height = input.nextInt();
        if( height < 0){
         System.out.print("Sorry, please make sure you have a positive integer: ");
         trulyInt = false;
        }
      }
     //If trulyInt is false then get rid of the string through junkstring
      else{
      System.out.print("Sorry, please make sure you have a positive integer: ");
      String junk = input.next();
      }
    }while( !trulyInt );
    //asking for the user to input a positive integer for the width of the argyle diamonds.
    System.out.print("please enter a positive integer for the width of the argyle diamonds: ");
    do{
     //Checking if the input is a integer and if so then if it is positive or not
      trulyInt = input.hasNextInt();
      if( trulyInt == true){
        widthofArgyleD = input.nextInt();
        if( widthofArgyleD < 0){
         System.out.print("Sorry, please make sure you have a positive integer: ");
         trulyInt = false;
        }
      }
      else{
        System.out.print("Sorry, please make sure you have a positive integer: ");
        String junk = input.next();
      }  
    }while( !trulyInt );
    //Asking for the width of the argyle center stripe.
    System.out.print("please enter a positive odd integer for the width of the argyle center stripe: ");
    do{
      //checking if the input is a integer, and if so store it has the widthofStripe. Then checks if the stored value is odd or even.
      trulyInt = input.hasNextInt();
      if ( trulyInt == true ){
        widthofStripe = input.nextInt();
        odd = widthofStripe%2;
        //If its even do this:
        if( odd == 0 || widthofStripe < 0){
          System.out.print("Sorry, please make sure you input a positive odd integer: ");
        }
     //making sure the widthofStripe is less than half of the widthofArgyleDiamonds
        if( widthofStripe > widthofArgyleD/2){
            System.out.print("Remember that the width needs to be less than half of width of argyle diamonds: ");
        } 
      }
      //If not a integer run this code and store that dummy string
      else{
        System.out.print("Sorry, please make sure you input a positive odd integer: ");
        String junk = input.next();
      }  
    }while( !trulyInt || widthofStripe < 0 || odd == 0 || widthofStripe > widthofArgyleD/2);
    // Asking the user for the character for each pattern type and then use the built in method to etractx the first character in the string
    // for each request
    System.out.print("please input a first character for the pattern fill: ");
    String firstCharString = input.next();
    char firstchar = firstCharString.charAt(0);
    System.out.print("please input a second character for the pattern fill: ");
    String secondCharString = input.next();
    char secondChar = secondCharString.charAt(0);
    System.out.print("please input a third character for the stripe fill:  ");
    String thirdCharString = input.next();
    char thirdChar = thirdCharString.charAt(0);
    
    /* These are all my counters for my loops that determine both the argle diamond, and the X. I have some counters like midpoint that are known values, but are
    meant to shrink over time. The idea of my code is that i create a numberline for my values of the loop where the counter of the loop has certain bounds that grow or shrink after every interation of i
    There are several loops in my code, the first loop is meant to get the total height of the window. The following loop that cotains a loop is meant to produce how many times
    the complete pattern. The following loop afterwards is meant to capture the rest of the width of the window. The code following is showing how my counters change per interation. Finally the if statementrs are meant to reset the counters
    that I need to be reset for the next cycle of the pattern.
    */
    int CounterofspreadLeft =1;
    int CounterofspreadRight = 0;
    int center = widthofArgyleD;
    int aftermidpoint = center+1;
    int midpoint = center;
    int counterForArgyleD = 0;
    int widthofX = 2*widthofArgyleD;
    int spread = widthofStripe/2;
    double repeatancehole =  widthofWindow/(2.0*widthofArgyleD);
    int repeatanceint =(int) repeatancehole;
    int counterofheight = 1;
    for(int i = 1; i <= height; ++i){
       for(int q = 1; q <= repeatanceint; ++q){

        for(int j = 1; j<= 2*widthofArgyleD; ++j){
          if( j >= CounterofspreadLeft - spread && j <= CounterofspreadLeft + spread){
            System.out.print(thirdChar);
          }
          else if( j <= 2*widthofArgyleD - CounterofspreadRight + spread && j >= 2*widthofArgyleD - CounterofspreadRight - spread){
             System.out.print(thirdChar);
          }
          else if( j >= aftermidpoint && j <= center && j >= center - widthofArgyleD && counterofheight<= widthofArgyleD + 1){
            System.out.print(secondChar);
          }
          else if( j <= midpoint && j >= center + 1&& j <= center + widthofArgyleD&&  counterofheight<= widthofArgyleD + 1){
            System.out.print(secondChar);
          }
          else if( j > counterForArgyleD && j <= center && j >= center - widthofArgyleD && counterofheight >= widthofArgyleD +2){
            System.out.print(secondChar);
          }
          else if( j <= widthofX && j >= center + 1 && j <= center + widthofArgyleD &&  counterofheight >= widthofArgyleD + 2){
            System.out.print(secondChar);
          }
          else{
            System.out.print(firstchar);
          } 
        }
      }
      for(int j = 1; j<= widthofWindow - repeatanceint*2*widthofArgyleD; ++j){
        if( j >= CounterofspreadLeft - spread && j <= CounterofspreadLeft + spread){
          System.out.print(thirdChar);
        }
        else if( j <= 2*widthofArgyleD - CounterofspreadRight + spread && j >= 2*widthofArgyleD - CounterofspreadRight - spread){
           System.out.print(thirdChar);
        }
        else if( j >= aftermidpoint && j <= center && j >= center - widthofArgyleD && counterofheight<= widthofArgyleD + 1){
          System.out.print(secondChar);
        }
        else if( j <= midpoint && j >= center + 1 && j <= center + widthofArgyleD&&  counterofheight<= widthofArgyleD + 1){
          System.out.print(secondChar);
        }
        else if( j > counterForArgyleD && j <= center && j >= center - widthofArgyleD && counterofheight >= widthofArgyleD +2){
          System.out.print(secondChar);
        }
        else if( j <= widthofX && j >= center + 1 && j <= center + widthofArgyleD &&  counterofheight >= widthofArgyleD + 2){
          System.out.print(secondChar);
        }
        else{
          System.out.print(firstchar);
        }
      }
      CounterofspreadLeft++;
      CounterofspreadRight++;
      --aftermidpoint;
      ++midpoint;
      if ( counterofheight >= widthofArgyleD+1 ){
        ++counterForArgyleD;
        --widthofX;
      }
      System.out.print("\n");
      counterofheight++;
      //Everytime the program completes a X 
      
      if ( i%(2*widthofArgyleD) == 0 ){
          CounterofspreadLeft =1;
          CounterofspreadRight = 0;
          center = widthofArgyleD;
          aftermidpoint = center+1;
          midpoint = center;
          counterForArgyleD = 0;
          widthofX = 2*widthofArgyleD;
          counterofheight  = 1;
      }
    }
  }
}