//Felix Quintana

import java.util.Scanner;
import java.util.Random;
public class Story{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    boolean getoutofLoop = false;
    boolean acceptableAnswer = false;
    String adjective; 
    String nounForSub;  
    String nounForObj; 
    String PastTenseVerb;
    
    do{
       adjective = adjectives();
       nounForSub= NonprimaryNounForSub();
       nounForObj = NonprimaryNounForObj();
       PastTenseVerb = PastTense();
      System.out.println( "The " + adjective + ' ' + nounForSub + ' ' + PastTenseVerb + " the " + adjectives() + ' ' + nounForObj + '.' );
      do{
        System.out.print("Would you like a new topic sentence? If so type Yes. If not type No ");
        String answer = input.nextLine();
        if( "No".equals(answer)){
          getoutofLoop = true;
          break;
        }
        if( "Yes".equals(answer)){
          getoutofLoop = false;
          break;
        }
        else{
          System.out.println("Sorry that was not a acceptable response");
          continue;
        }
      }while(acceptableAnswer == false);
    }while(!getoutofLoop);
    paragraph( adjective, nounForSub, nounForObj, PastTenseVerb);
    
  }

  public static String adjectives(){
    String adj = "junk";
    int randomInt = RandomGen();
    switch(randomInt){
      case 0: adj = "fast";
              break;
      case 1: adj = "slow";
              break;
      case 2: adj = "fancy";
              break;
      case 3: adj = "clever";
              break;
      case 4: adj = "funny";
              break;
      case 5: adj = "important";
              break;
      case 6: adj = "tall";
             break;
      case 7: adj = "short";
              break;
      case 8: adj = "strong";
              break;
      case 9: adj = "weary";
              break;
    }
    return adj;
  }
  public static String NonprimaryNounForSub(){
    String noun = "junk";
    int randomInt = RandomGen();
    switch(randomInt){
      case 0: noun = "fox";
              break;
      case 1: noun = "crab";
              break;
      case 2: noun = "monster";
              break;
      case 3: noun = "runner";
              break;
      case 4: noun = "bucket";
              break;
      case 5: noun = "bunny";
              break;
      case 6: noun = "brain";
             break;
      case 7: noun = "cat";
              break;
      case 8: noun = "man";
              break;
      case 9: noun = "actor";
              break;
    }
    return noun;
  }
  public static String PastTense(){
    String PastTenseVerb = "junk";
    int randomInt = RandomGen();
    switch(randomInt){
      case 0: PastTenseVerb = "accepted";
              break;
      case 1: PastTenseVerb = "arrived";
              PastTenseVerb += " to";
              break;
      case 2: PastTenseVerb = "bought";
              break;
      case 3: PastTenseVerb = "behaved";
              PastTenseVerb += " for";
              break;
      case 4: PastTenseVerb = "calculated";
              break;
      case 5: PastTenseVerb = "carried";
              break;
      case 6: PastTenseVerb = "contained";
             break;
      case 7: PastTenseVerb = "ran";
              PastTenseVerb += " to";
              break;
      case 8: PastTenseVerb = "flew";
              PastTenseVerb += " to";
              break;
      case 9: PastTenseVerb = "delivered";
              break;
    }
    return PastTenseVerb;
  }
  public static String NonprimaryNounForObj(){
      int randomInt = RandomGen();
      String Nounforobj = "junk";
      switch(randomInt){
        case 0: Nounforobj = "mailbox";
                break;
        case 1: Nounforobj = "race";
                break;
        case 2: Nounforobj = "sun";
                break;
        case 3: Nounforobj = "man";
                break;
        case 4: Nounforobj = "jar";
                break;
        case 5: Nounforobj = "universe";
                break;
        case 6: Nounforobj = "plants";
                break;
        case 7: Nounforobj = "college";
                break;
        case 8: Nounforobj = "school";
                break;
        case 9: Nounforobj = "electronics";
                break;
      }
      return Nounforobj;
  }
  public static int RandomGen(){
    Random random = new Random();
    int randomInt = random.nextInt(10);
    return randomInt;
  }
  public static String FirstSentenceGen( String adjective, String nounForSub , String nounForObj, String PastTenseVerb){
    System.out.println( "The " + adjective + ' ' + nounForSub + ' ' + PastTenseVerb + " the " + adjectives() + ' ' + nounForObj + '.' );
    return nounForSub;
  }
  public static String Secondsentence( String Subject){
    Random random = new Random();
    int randomInt = random.nextInt(5);
    if( randomInt <=2){
      Subject = "It";
    }
  //  System.out.println(randomInt);
    if( "It".equals(Subject) ){
      System.out.println(Subject + " " + PastTense() + " " + "the"+ " "+ adjectives() +  " " +NonprimaryNounForObj())+ '.';
    }
    else{
      System.out.println("The"+' '+Subject + " " + PastTense() + " the " +  adjectives() +  " " +NonprimaryNounForObj() + '.');
    }
      return  Subject;
  }
  public static void Conclusion(String Subject){
    System.out.print(Subject +  " " + PastTense() + " " +  adjectives() +  " " +NonprimaryNounForObj());
    return;
  }
  public static void paragraph( String adjective, String nounForSub , String nounForObj, String PastTenseVerb){
    Conclusion(Secondsentence(Secondsentence(Secondsentence(Secondsentence(FirstSentenceGen(adjective,  nounForSub , nounForObj, PastTenseVerb ))))));
  }
  
}