/* Felix Quintana
CSE2
objectives: The objective of this code is to have an array of 53 elements to represent a deck of cards. I then use a function to shuffle up my cards.objectives
I then hand out the ten cards to the two players. Thereafter, I determine what they have in their hands by several functions, and print out who won
*/
import java.util.Random;
public class DrawPoker{
  public static void main(String[] args){
    int score  = 0;  //Initialize these variables for the score of each hand.
    int score2 = 0;
    System.out.println("Hello, welcome to poker!");
    int deck[] = new int[52];                          //I construct my deck of cards and put it in a function called shuffle to shuffle the cards.
    for( int i = 0; i < deck.length; i++){
      deck[i] = i;
    } 
    int randomDeck[] = shuffle(deck);
    int playerOneHand[] = new int [5];
    int playerTwoHand[] = new int [5];  //These are my two constructed players hands
    for(int i = 0; i < 10; i++){
      if(i%2 == 0){                      //If the index is even itll give the card to first person, and if odd it will give it to the second.
        playerOneHand[i-i/2] = randomDeck[i];
      }
      else{
        playerTwoHand[i- (i+1)/2] = randomDeck[i];
      }
    }
    System.out.print("Player one hand: ");   //Prints out what each hand is.
    for( int m = 0; m< playerOneHand.length; m++){
    System.out.print(playerOneHand[m] + " ");
    }
    System.out.println();
    System.out.print("Player two hand: "); 
    for( int j = 0; j< playerTwoHand.length; j++){
    System.out.print(playerTwoHand[j] + " ");
    }
    System.out.println();
    boolean trulypair = pair(playerOneHand);  //This is where it will run all the functions to test if it is a pair threeOfKind or flush or full House.
    if( trulypair == true){        //If it meets any of the creteria then it will add a score to the player, and whoever has the higher score wins.
      score = 1;
    }
    boolean trulypair2 = pair(playerTwoHand);
      if( trulypair2 == true){
      score2 = 1;
    }
    boolean trulyThreeOfKind = threeOfKind(playerOneHand);
    if( trulyThreeOfKind == true){
      score = 2;
    }
    boolean trulyThreeOfKind2 = threeOfKind(playerTwoHand);
        if( trulyThreeOfKind2 == true){
      score2 = 2;
    }
    boolean trulyflush = flush(playerOneHand);
        if( trulyflush == true){
      score = 3;
    }
    boolean trulyflush2 = flush(playerTwoHand);
     if( trulyflush2 == true){
      score2 = 3;
    }
    boolean trulyFullHouse = fullHouse(playerOneHand);
     if( trulyFullHouse == true){
      score = 4;
    }
    boolean trulyFullHouse2 = fullHouse(playerTwoHand);
     if( trulyFullHouse2 == true){
      score2 = 4;
    }
    if( score > score2){
      System.out.println("Player 1 wins!");
    }
    else if( score2 > score){
      System.out.println("Player 2 wins!");
    }
    else{
      System.out.println("Tie :( ");
    }
  }
  public static int[] shuffle( int originalDeck[] ){
    Random rand = new Random();
    int randomNum;
    int randomDeck[] = originalDeck; 
    /*I relabel my array just out of convienence. I then make a random number inside my for loop so that for every element a random number is generated. 
    I then enclose it all in a do while loop to make sure that none of the previously generated numbers are what has been generated. If so it will loop around for the same element
    until its the number I am looking for. 
    */
    boolean trulytrue = true;
    for( int i = 0; i < originalDeck.length; i++){
      do{
        trulytrue = true;
        randomNum = rand.nextInt(52);
        randomDeck[i] = randomNum;
       
        if ( i > 0){
          for( int k = i; k > 0; k--){
            if( randomDeck[k-1] == randomDeck[i]){
              trulytrue = false;
              break;
            }
          }
        }
      }while( trulytrue == false);
    }
    return randomDeck;        // return the array          
  }
  public static boolean pair(int deck[]){
    int counter = 0;
    //This is the method I use to determine if it is a pair, and with a similar construction finding if it is a the of a kind.
    //So I make a for loop checking all the other cards starting from the card location I am, and I dont repeat checking certain combinations, but that each combination is unique.
    for(int i = 0; i < deck.length; i++){
      counter = 0;
      for( int k = 0; k < deck.length -1 - i; k++ ){
        if( deck[i]%13 == deck[k+ 1 + i]%13){
          counter += 1;
        }
         if( counter == 1){     //THIS IS THE CRUICUAL STAGE. If the loop detects there are only one pairing then it will be considered as a pair and return true.
           return true;
         }
      }
    }
    return false;
  }
    // 
  public static boolean threeOfKind(int deck[]){   //This function is very similar to the pair function, but the counter is what changed to insure it is a three of kind.
    int counter = 0;
    for(int i = 0; i < deck.length; i++){
      counter = 0;
      for( int k = 0; k < deck.length -1 - i; k++ ){
        if( deck[i]%13 == deck[k+ 1 + i]%13){
          counter += 1;
        }
        if( counter == 2){
          return true;
        }
      }
    }
    return false;
  }
  public static boolean flush( int deck[]){  //This one was of the easier functions. All I had to do was check if the card at hand was the same suit as the ones previous to that card.
    int counter = 0;
    for( int i =0; i < deck.length; i++){
      for( int k = 0; k < deck.length - 1 - i; k++){
        if( deck[i]/13 == deck[i+k+1]/13 ){  // Two loops allow me to choose one card as the reference to check all the other cards, and without having to waste already configured combinations.
          counter +=1;          
        }
        if( counter == 4){
          return true;
        }
      }
    }
    return false;
  }
  public static boolean fullHouse( int deck[]){
    boolean ifPair = pair(deck);
    boolean ifThreeOfKind = threeOfKind(deck);  // If there is a pair and three of a kind found than there must be a full house.
    if ( ifPair == true && ifThreeOfKind == true){
      return true;
    }
    else{
      return false;
    }
  }
}